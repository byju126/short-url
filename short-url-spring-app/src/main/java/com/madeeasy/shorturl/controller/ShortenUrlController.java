package com.madeeasy.shorturl.controller;

import com.madeeasy.shorturl.dto.ShortenUrlRequest;
import com.madeeasy.shorturl.dto.ShortenUrlResponse;
import com.madeeasy.shorturl.service.ShortenUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
public class ShortenUrlController {

    @Autowired
    private ShortenUrlService shortenUrlService;

    @RequestMapping(value="/{shortenedUrl}", method = RequestMethod.GET )
    public ResponseEntity<Object> redirect(@PathVariable("shortenedUrl") String shortenedUrl) throws URISyntaxException {
        String originalUrl = shortenUrlService.getOriginalUrl(shortenedUrl);
        URI uri = new URI(originalUrl);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(uri);
        return new ResponseEntity<>(httpHeaders, HttpStatus.SEE_OTHER);
    }

    @RequestMapping(value="/shorten", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin(origins = {"http://localhost:3000", "http://localhost:5000"})
    public ShortenUrlResponse shortenUrl(@RequestBody ShortenUrlRequest urlRequest){
        return shortenUrlService.shortenUrl(urlRequest.getUrl());
    }
}
