package com.madeeasy.shorturl.persistance.repository;

import com.madeeasy.shorturl.persistance.entity.ShortenUrl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ShortenUrlRepository extends JpaRepository<ShortenUrl, Long> {
    ShortenUrl findByShortenUrl(String shortUrl);
    ShortenUrl findByOriginalUrl(String originalUrl);

    @Query("select max(a.id) from ShortenUrl a")
    Long maxIdValue();
}
