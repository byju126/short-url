package com.madeeasy.shorturl.service;

public interface UrlUniqueIdGeneratorService {
    public String getUniqueId(String url);
}
