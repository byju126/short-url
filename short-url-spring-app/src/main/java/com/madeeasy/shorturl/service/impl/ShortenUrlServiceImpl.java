package com.madeeasy.shorturl.service.impl;

import com.madeeasy.shorturl.dto.ShortenUrlResponse;
import com.madeeasy.shorturl.persistance.entity.ShortenUrl;
import com.madeeasy.shorturl.persistance.repository.ShortenUrlRepository;
import com.madeeasy.shorturl.service.ShortenUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ShortenUrlServiceImpl implements ShortenUrlService {


    @Autowired
    private ShortenUrlRepository shortenUrlRepository;

    @Autowired
    private UrlUniqueIdGeneratorServiceImpl urlUniqueIdGeneratorService;

    @Override
    public ShortenUrlResponse shortenUrl(String url) {
        ShortenUrl shortenUrl = shortenUrlRepository.findByOriginalUrl(url);
        if(shortenUrl == null){
            shortenUrl = new ShortenUrl();
            shortenUrl.setOriginalUrl(url);
            shortenUrl.setShortenUrl(urlUniqueIdGeneratorService.getUniqueId(url));
            shortenUrlRepository.saveAndFlush(shortenUrl);
        }
        return new ShortenUrlResponse(shortenUrl.getOriginalUrl(), shortenUrl.getShortenUrl());
    }

    @Override
    public String getOriginalUrl(String url) {
        ShortenUrl shortenUrl = shortenUrlRepository.findByShortenUrl(url);
        if(shortenUrl == null ){
            return url;
        }
        return shortenUrl.getOriginalUrl();
    }
}
