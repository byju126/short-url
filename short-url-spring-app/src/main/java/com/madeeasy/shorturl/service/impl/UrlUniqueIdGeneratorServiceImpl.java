package com.madeeasy.shorturl.service.impl;

import com.madeeasy.shorturl.persistance.repository.ShortenUrlRepository;
import com.madeeasy.shorturl.service.UrlUniqueIdGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicLong;

/*
  Currenlty we are implementing a unique Id generator a sequencialId generator using as AtomiLong conter
  and this need to be changed if we are installing the service on multiple machines for horizontal scalability
  This Id can be generated as unique using any method and only condition is that id should be unique and for
  the given urls and a easy to read simple one.
 */
@Component
public class UrlUniqueIdGeneratorServiceImpl implements UrlUniqueIdGeneratorService {

    private AtomicLong atomicLong = new AtomicLong(0l);
    private volatile boolean firstTime = true;

    @Autowired
    private ShortenUrlRepository shortenUrlRepository;

    @Override
    public String getUniqueId(String url) {
        if(firstTime){
            firstTime = false;
            Long maxId = shortenUrlRepository.maxIdValue();
            if(maxId!= null) {
                atomicLong.set(maxId);
            }
        }
        return Long.toString(atomicLong.addAndGet(1l));
    }
}
