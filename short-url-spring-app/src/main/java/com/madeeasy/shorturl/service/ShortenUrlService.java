package com.madeeasy.shorturl.service;

import com.madeeasy.shorturl.dto.ShortenUrlResponse;

public interface ShortenUrlService {
    ShortenUrlResponse shortenUrl(String url);
    String getOriginalUrl(String shortenUrl);
}
