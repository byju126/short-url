package com.madeeasy.shorturl.dto;

public class ShortenUrlResponse {

    private String url;
    private String shortenUrl;

    public ShortenUrlResponse() {
    }

    public ShortenUrlResponse(String url, String shortenUrl) {
        this.url = url;
        this.shortenUrl = shortenUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShortenUrl() {
        return shortenUrl;
    }

    public void setShortenUrl(String shortenUrl) {
        this.shortenUrl = shortenUrl;
    }
}
