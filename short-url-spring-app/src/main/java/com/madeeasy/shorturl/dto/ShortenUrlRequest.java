package com.madeeasy.shorturl.dto;

public class ShortenUrlRequest {

    private String url;

    public ShortenUrlRequest() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
