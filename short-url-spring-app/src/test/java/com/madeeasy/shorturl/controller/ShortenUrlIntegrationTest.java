package com.madeeasy.shorturl.controller;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.net.URL;
import java.util.Random;

import com.madeeasy.shorturl.service.ShortenUrlService;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.madeeasy.shorturl.dto.ShortenUrlRequest;
import com.madeeasy.shorturl.dto.ShortenUrlResponse;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ShortenUrlIntegrationTest {

    @LocalServerPort
    private int port;

    private URL base;

    @Autowired
    private TestRestTemplate template;

    @Autowired
    private ShortenUrlService shortenUrlService;
    Random rand = new Random();

    @Before
    public void setUp() throws Exception {
        this.base = new URL("http://localhost:" + port + "/");
    }

    @Test
    public void testShortenUrl() {
        int randomLength = getRandom(10, 5000);
        String url = RandomStringUtils.random(randomLength, true, true);
        ShortenUrlResponse shortenUrlResponse = shortenUrl(url);
        assertThat(shortenUrlResponse.getUrl(), equalTo(url));
        String originalUrl = shortenUrlService.getOriginalUrl(shortenUrlResponse.getShortenUrl());
        assertThat(originalUrl, equalTo(url));
    }

    @Test(expected = org.springframework.web.client.ResourceAccessException.class)
    public void testRedirection() throws Exception {
        int randomLength = getRandom(10, 5000);
        boolean useLetters = true;
        boolean useNumbers = false;
        String url = "http://notahost/" + RandomStringUtils.random(randomLength, useLetters, useNumbers);
        ShortenUrlResponse shortenUrlResponse = shortenUrl(url);
        assertThat(shortenUrlResponse.getUrl(), equalTo(url));
        ResponseEntity<String> response = template.getForEntity(base.toString() + "/" + shortenUrlResponse.getShortenUrl(), String.class);
    }

    private ShortenUrlResponse shortenUrl(String url) {
        HttpHeaders headers = new HttpHeaders();
        ShortenUrlRequest shortenUrlRequest = new ShortenUrlRequest();
        shortenUrlRequest.setUrl(url);
        HttpEntity<ShortenUrlRequest> request = new HttpEntity<>(shortenUrlRequest, headers);
        ResponseEntity<ShortenUrlResponse> result = this.template.postForEntity(base.toString() + "/shorten", request, ShortenUrlResponse.class);
        return result.getBody();
    }

    private Integer getRandom(int min, int max) {
        return rand.nextInt((max - min) + 1) + min;
    }
}
