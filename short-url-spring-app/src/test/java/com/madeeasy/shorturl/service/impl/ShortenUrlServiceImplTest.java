package com.madeeasy.shorturl.service.impl;

import com.madeeasy.shorturl.dto.ShortenUrlResponse;
import com.madeeasy.shorturl.persistance.entity.ShortenUrl;
import com.madeeasy.shorturl.persistance.repository.ShortenUrlRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ShortenUrlServiceImplTest {

    @InjectMocks
    private ShortenUrlServiceImpl shortenUrlService;

    @Mock
    private ShortenUrlRepository shortenUrlRepository;

    @Mock
    private UrlUniqueIdGeneratorServiceImpl urlUniqueIdGeneratorService;

    @Test
    public void testShortenUrl(){
        String url = RandomStringUtils.random(100,  true, true);
        String expectedShortnedUrl = "shortned";
        when(urlUniqueIdGeneratorService.getUniqueId(url)).thenReturn(expectedShortnedUrl);
        ShortenUrlResponse shortenUrlResponse = shortenUrlService.shortenUrl(url);
        assertThat(shortenUrlResponse.getUrl(), equalTo(url));
        assertThat(shortenUrlResponse.getShortenUrl(), equalTo(expectedShortnedUrl));
        verify(shortenUrlRepository, times(1)).findByOriginalUrl(url);
        verify(urlUniqueIdGeneratorService, times(1)).getUniqueId(url);
        verify(shortenUrlRepository, times(1)).saveAndFlush(any());
    }

    @Test
    public void testGetOriginalUrl(){
        String url = RandomStringUtils.random(100,  true, true);
        String expectedShortnedUrl = "shortned";
        ShortenUrl shortenUrl = new ShortenUrl();
        shortenUrl.setShortenUrl(expectedShortnedUrl);
        shortenUrl.setOriginalUrl(url);
        when(shortenUrlRepository.findByShortenUrl(expectedShortnedUrl)).thenReturn(shortenUrl);
        String actualOriginalUrl = shortenUrlService.getOriginalUrl(expectedShortnedUrl);
        assertThat(actualOriginalUrl, equalTo(url));
        verify(shortenUrlRepository, times(1)).findByShortenUrl(expectedShortnedUrl);
    }


}