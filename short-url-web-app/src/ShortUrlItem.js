import React, { Component } from 'react'
import { properties } from './properties.js'

class ShortUrlItem extends Component {

    render() {
        const url = properties.appUrl + "/" + this.props.shortUrl
        return (
            this.props.originalUrl &&
            <div>
                <label>Url:{this.props.originalUrl}</label> <br/>
                <label>Short Url:{this.props.shortUrl}</label> <br />
                <label> The above url can be accessed by {url}</label>

            </div>
        );
    }
}

export default ShortUrlItem;