import React from 'react';
import ReactDOM from 'react-dom';
import ShortUrlApp from './ShortUrlApp';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ShortUrlApp />, div);
  ReactDOM.unmountComponentAtNode(div);
});
