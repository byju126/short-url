import React, { Component } from 'react'
import ShortUrlItem from './ShortUrlItem'
import { properties } from './properties.js'


class ShortUrlApp extends Component {
    constructor(){
        super()
        this.state = {
            url: "",
            originalUrl: "",
            shortUrl:""
        }
        this.onChange = this.onChange.bind(this)
        this.keyPressed = this.keyPressed.bind(this)
    }

    onChange(event){
        const {name, value} = event.target
        this.setState({ [name]: value})
    }

    keyPressed(event){
        if(event.keyCode === 13){
            const that = this
            fetch(properties.appUrl + '/shorten', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({url: this.state.url})
            }).then(function(response) {
                return response.json();
            }).then(function(data) {
                that.setState({
                    url:"",
                    originalUrl: data.url,
                    shortUrl:data.shortenUrl
                })
            });
        }
    }

    render() {
        return (
            <div>
                <h3> Url Shortening App </h3> <br />
                <label>Enter the url : </label>
                <input name="url" value={this.state.url} onKeyDown={this.keyPressed}  onChange={this.onChange} style={{width: "370px"}}/>
                <br />
                <ShortUrlItem originalUrl={this.state.originalUrl} shortUrl={this.state.shortUrl} />
            </div>
        );
    }
}

export default ShortUrlApp;