This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Introduction
Short Url app has the following requirement
 1. A user should be able to load the index page of your site and be presented with an input field where they can enter a URL.
2. Upon entering the URL, a "shortened" version of that url is created and shown to the user as a URL to the site you're building.
3. When visiting that "shortened" version of the URL, the user is redirected to the original URL.
4. Additionally, if a URL has already been shortened by the system, and it is entered a second time, the first shortened URL should be given back to the user.
For example, if I enter http://www.apple.com/iphone-7/ into the input field, and I'm running the app locally on port 9000, I'd expect to be given back a URL that looked something like http://localhost:9000/1. Then when I visit http://localhost:9000/1, I am redirected to http://www.apple.com/iphone-7/.

### Pre-requisite
inode, npm, JDK and maven with the following version need to be installed

node version v11.14.0

npm version 6.9.0

JDK 1.8 

Apache Maven 3.6.1

### Projects

There are two projects 1. short-url-spring-app 2. short-url-web-app 

###  short-url-spring-app

This project has been developed using  Java 8, Spring, JPA and H2 (in memory data base). Used JPA to easily switch over to any other DBs.

go to the folder short-url-spring-app and run `mvn clean install` this will compile, build and run all the test cases of the application.

The application can be launched by running `mvn package && java -jar target/short-url-spring-app-0.1.0.jar`

Currently the web service app is starting at the port 8080 and if there are any changes please update the app url in  short-url-web-app/src/properties.js file

###     short-url-web-app
This web project has been launched using npm

go to short-url-web-app folder and run `npm install` this will download all the required node modules to run the react application.

Web can be launched by running `npm start`

Please follow the the console of `npm start` if we need to production ready the code




